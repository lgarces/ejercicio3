<%-- 
    Document   : index
    Created on : 08-abr-2021, 20:08:23
    Author     : GaCTuS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Acceso Sistema Web</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand">
    <link rel="stylesheet" href="css/styles.min.css">
    <script type="text/javascript" src="js/validaciones.js"></script>
    <link type="text/css" rel="stylesheet" href="css/estilos.css" media="screen" /></link>
</head>

<body>
    <form id="frmLogin" name="frmLogin" style="font-family:Quicksand, sans-serif;background-color:rgba(230,230,230,0.73);width:320px;padding:40px;" method="post" action="LoginController">
        <h3 id="head" style="color:rgb(97,105,107);"><b>Ingreso Usuarios</b></h1>
        <div class="text-center">
            <img class="rounded img-fluid" src="imagenes/login.png" id="image" style="width:auto;height:auto;">
        </div>
        <div class="form-group">
            <input class="form-control" type="email" id="txtUsuario" placeholder="Usuario" onfocus="fnLimpiarErrores();" name="txtUsuario">
        </div>
        <div class="form-group">
            <input class="form-control" type="password" id="txtPassword" placeholder="Contraseña" onfocus="fnLimpiarErrores();" name="txtPassword">
        </div>
        <div class="form-group"></div>
        <button class="btn btn-light" type="button" id="butonas" style="width:100%;height:100%;margin-bottom:10px;background-color:rgb(201,137,16, 0.8);" onclick="fnValidaLogin();">Ingresar</button><a href="#" id="linkas" style="font-size:12px;margin:auto;margin-left:0;margin-right:0;margin-bottom:0;margin-top:0;padding-left:0;padding-right:0;color:rgb(177,151,70);"><b>¿Olvidó su password?</b></a>
        <div id="errores" class="textosError"></div>
    </form>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>
