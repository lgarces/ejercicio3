	//creo mi array con los usuarios que pueden ingresar
	var listadoUsuarios = ['aliberona','dorellana','ctorres','cgonzalez','cmoraga','ediaz','egalvez','fyevenes','gpavez','jaguilar','lgarces','jsuarez','jbilbao','svasquez','ttoledo','rsanmartin']
	//en esta función valido los usuarios
	function fnValidaLogin(){
		var usuario = document.getElementById('txtUsuario').value;	
		var pass = document.getElementById('txtPassword').value;
		var url = "";
		var error = false;
		var mensaje = '';
		if (usuario == ''){
			error = true;
			mensaje = mensaje + '<li>Debe ingresar el usuario</li>';
		}
		if (pass == ''){
			error = true;
			mensaje = mensaje + '<li>Debe ingresar el password</li>';
		}
		if (!listadoUsuarios.includes(usuario)){
			error = true;
			mensaje = mensaje + '<li>Nombre de usuario/password incorrectos</li>';
		}
		if (error){
			document.getElementById('errores').innerHTML = '<fieldset class="anchoLegendLogin"><legend>Han ocurrido los siguientes errores</legend><ul>' + mensaje + '</ul></fieldset>';
		}else{
			if(listadoUsuarios.includes(usuario)){
                            document.frmLogin.submit();
			}
		}
	}
	//aquí redirijo al login.
    function fnSalir(){
		window.location.href = "frmLogin.html";
    }
    //Aqui limpio los errores al intentar loguear
    function fnLimpiarErrores(){
    	document.getElementById('errores').innerHTML = ""
    }
    //Validación de rut (se puede modificar para estudiantes con pasaporte u otro tipo de documento)
	function fnRut(rutCompleto)
	{
		rutOk = rutCompleto.indexOf("-");
		arrRut = rutCompleto.split('-');
		if (rutOk != -1){
			ruta = arrRut[0];
			dv = arrRut[1];
			var count = 0;
			var count2 = 0;
			var factor = 2;
			var suma = 0;
			var sum = 0;
			var digito = 0;
			count2 = ruta.length - 1;	 
			while(count < ruta.length)
			{
				sum = factor * (parseInt(ruta.substr(count2,1))); 
				suma = suma + sum;
				sum=0;
				count = count + 1;
				count2 = count2 - 1;
				factor = factor + 1;
													   
				if(factor > 7)
				{
					factor=2; 
				} 
													  
			}										   
			digito= 11 - (suma % 11)											   
			if(digito==11)
			{
				digito=0;
			}											   
			if(digito==10)
			{
				digito="K";
			}											   
			if(digito==dv.toUpperCase())
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}else{
			return 1;
		}		
	}
//	Calculo la edad del estudiante
	function calcularEdad(FechaNacimiento) {
	    var fechaNace = new Date(FechaNacimiento);
	    var fechaActual = new Date()
	    var mes = fechaActual.getMonth();
	    var dia = fechaActual.getDate();
	    var ano = fechaActual.getFullYear();
	    fechaActual.setDate(dia);
	    fechaActual.setMonth(mes);
	    fechaActual.setFullYear(ano);
	    edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));
	    if (isNaN(edad)){
	    	edad = '';
	    }
	    document.getElementById('txtEdad').value = edad;
	}
	//Válido el ingreso de un nuevo estudiante
	function fnValidaIngresoEstudiante(){
		error = '';
		nombreEstudiante = document.getElementById('txtNombre').value;
		rutEstudiante = document.getElementById('txtRut').value;
		fechaNacimiento = document.getElementById('txtFechaNacimiento').value;
		direccionEstudiante = document.getElementById('txtDireccion').value;
		paisEstudiante = document.getElementById('lsPaises').value;
		telefonoEstudiante = document.getElementById('txtTelefono').value;
		cursoEstudiante = document.getElementById('txtCurso').value;

		if (nombreEstudiante == ''){
			error = error + '<li>El campo Nombre estudiante es requerido</li>';
		}
		if (rutEstudiante == ''){
			error = error + '<li>El campo Rut estudiante es requerido</li>';
		}else{
			if (fnRut(rutEstudiante) == 1){
				error = error + '<li>El rut ingresado no es válido</li>';
			}
		}
		if (fechaNacimiento == ''){
			error = error + '<li>El campo Fecha Nacimiento es requerido</li>';
		}
		if (direccionEstudiante == ''){
			error = error + '<li>El campo Dirección Estudiante es requerido</li>';
		}
		if (paisEstudiante == ''){
			error = error + '<li>El campo Pais Estudiante es requerido</li>';
		}
		if (telefonoEstudiante == ''){
			error = error + '<li>El campo Teléfono Estudiante es requerido</li>';
		}else{
			if (isNaN(telefonoEstudiante)){
				error = error + '<li>El campo Teléfono Estudiante es incorrecto</li>';
			}
		}
		if (cursoEstudiante == ''){
			error = error + '<li>El campo Curso Estudiante es requerido</li>';
		}
		if (error != ''){
			document.getElementById('errores').innerHTML = '<fieldset class="anchoLegendUsuarios"><legend>Han ocurrido los siguientes errores:</legend>' + error + '</fieldset>';
		}else{
			alert('Datos guardados con éxito');
			fnLimpiaIngresoEstudiante();
			$('#modalIngreso').modal('hide');
		}
	}
	//Función para limpiar el formulario de ingreso de estudiantes
	function fnLimpiaIngresoEstudiante(){
		document.getElementById('txtNombre').value = '';
		document.getElementById('txtRut').value = '';
		document.getElementById('txtFechaNacimiento').value = '';
		document.getElementById('txtEdad').value = '';
		document.getElementById('txtDireccion').value = '';
		document.getElementById('lsPaises').value = '';
		document.getElementById('txtTelefono').value = '';
		document.getElementById('txtCurso').value = '';
		fnLimpiarErrores();
	}
/*	Comienzo las validación para el formulario de notas */
	//Valido el rango de notas
	function validaNotas(notaConsultar){
		if (!isNaN(notaConsultar)){ //Válido que la nota no contenga letras
			if (parseFloat(notaConsultar) < 1 || parseFloat(notaConsultar) > 7){ //valido que la nota esté dentro del rango
				return 1;
			}else{
				return 0;
			}
		}else{
			return 1;
		}
	}
	//Valido el ingreso de notas
	function fnValidaIngresoNotas(){
		var datoEstudiante = document.getElementById('lsEstudiantes').value;
		var primeraNota = document.getElementById('txtNota1').value;
		var segundaNota = document.getElementById('txtNota2').value;
		var terceraNota = document.getElementById('txtNota3').value;
		var error = '';
		if (datoEstudiante == ''){
			error = error + '<li>Debe seleccionar un Estudiante</li>';
		}
		if (primeraNota == '' && segundaNota == '' && terceraNota == ''){
			error = error + '<li>Debe ingresar al menos una nota';
		}else{
			if (primeraNota != ''){
				if (parseInt(validaNotas(primeraNota)) == 1){
					error = error + '<li>La Primera nota ingresada no es válida</li>';
				}
			}
			if (segundaNota != ''){
				if (parseInt(validaNotas(segundaNota)) == 1){
					error = error + '<li>La Segunda Nota ingresada no es válida</li>';
				}
			}
			if (terceraNota != ''){
				if (parseInt(validaNotas(terceraNota)) == 1){
					error = error + '<li>La Tercera Nota ingresada no es válida</li>';
				}
			}
		}
		if (error != ''){
			document.getElementById('errores').innerHTML = '<fieldset class="anchoLegendNotas"><legend>Han ocurrido los siguientes errores:</legend>' + error + '</fieldset>';
		}else{
			alert('Notas guardadas con éxito');
			fnLimpiaIngresoNotas();
			$('#modalNotas').modal('hide');
		}
	}
	function fnCalculaPromedio(){
		var primeraNota = document.getElementById('txtNota1').value;
		var segundaNota = document.getElementById('txtNota2').value;
		var terceraNota = document.getElementById('txtNota3').value;
		var sumaNotas = 0;
		var contadorNotas = 0;
		var error = false;
		if (parseInt(validaNotas(primeraNota)) == 0 && primeraNota != ''){
			contadorNotas = parseFloat(contadorNotas) + 1;
			sumaNotas = parseFloat(sumaNotas) + parseFloat(primeraNota);
		}
		if (parseInt(validaNotas(segundaNota)) == 0 && segundaNota != ''){
			contadorNotas = parseFloat(contadorNotas) + 1;
			sumaNotas = parseFloat(sumaNotas) + parseFloat(segundaNota);
		}
		if (parseInt(validaNotas(terceraNota)) == 0 && terceraNota != ''){
			contadorNotas = parseFloat(contadorNotas) + 1;
			sumaNotas = parseFloat(sumaNotas) + parseFloat(terceraNota);
		}
		if (contadorNotas != 0){
			var promedioEstudiante = (parseFloat(sumaNotas)/parseFloat(contadorNotas));
			document.getElementById('txtPromedio').value = promedioEstudiante.toFixed(1); 
		}else{
			document.getElementById('txtPromedio').value = '';
		}
	}
	function fnLimpiaIngresoNotas(){
		document.getElementById('lsEstudiantes').value = '';
		document.getElementById('txtNota1').value = '';
		document.getElementById('txtNota2').value = '';
		document.getElementById('txtNota3').value = '';
		document.getElementById('txtPromedio').value = '';
		fnLimpiarErrores();
	}
/*	Validaciones del formulario Curso */
	function fnValidaCursos(){
		var error = '';
		var nombreCurso = document.getElementById('txtCurso').value;
		var nombreEscuela = document.getElementById('lstEscuela').value;
		var tipoModalidad = document.getElementById('lstModalidad').value;
		var tipoStatus = document.getElementById('lstStatus').value;
		if (nombreCurso == ''){
			error = error + '<li>El campo Nombre Curso es requerido</li>';
		}
		if (nombreEscuela == ''){
			error = error + '<li>El campo Nombre Escuela es requerido</li>';
		}
		if (tipoModalidad == ''){
			error = error + '<li>El campo Modalidad Curso es requerido</li>';
		}
		if (tipoStatus == ''){
			error = error + '<li>El campo Status Curso es requerido</li>';
		}
		if (error != ''){
			document.getElementById('errores').innerHTML = '<fieldset class="anchoLegendUsuarios"><legend>Han ocurrido los siguientes errores:</legend>' + error + '</fieldset>';
		}else{
			alert('Datos guardados con éxito');
			fnLimpiaIngresoCursos();
			$('#modalCursos').modal('hide');
		}
	}
	function fnLimpiaIngresoCursos(){
		document.getElementById('txtCurso').value = '';
		document.getElementById('lstEscuela').value = '';
		document.getElementById('lstModalidad').value = '';
		document.getElementById('lstStatus').value = '';
		fnLimpiarErrores();
	}